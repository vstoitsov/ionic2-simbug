import { Component } from '@angular/core';
import { Sim } from 'ionic-native';

import { NavController } from 'ionic-angular';

@Component({
  templateUrl: 'home.html'
})
export class HomePage {
  public simInfo: any;
  constructor(public navCtrl: NavController) {
    console.log(Sim);
    Sim.getSimInfo().then(
      (info) => this.simInfo = info,
      (err) => console.log('Unable to get sim info: ', err)
    );

  }
}
